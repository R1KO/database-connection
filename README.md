[![pipeline status](https://gitlab.com/R1KO/database-connection/badges/master/pipeline.svg)](https://gitlab.com/R1KO/database-connection/-/commits/master) [![coverage report](https://gitlab.com/R1KO/database-connection/badges/master/coverage.svg)](https://gitlab.com/R1KO/database-connection/-/commits/master)

## Connection

### SQLite

```php
use R1KO\Database\ConnectionFactory;

$params = [
    'driver' => 'sqlite',
    'path'   => ':memory:'
];

$db = ConnectionFactory::create($params);
```

### MySQL
```php
use R1KO\Database\ConnectionFactory;

$params = [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'port'      => '3306',
    'database'  => 'database',
    'username'  => 'root',
    'password'  => 'password',
    'charset'   => 'utf8mb4',
];

$db = ConnectionFactory::create($params);
```


### MySQL with PDO options
```php
use R1KO\Database\ConnectionFactory;
use PDO;

$params = [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'port'      => '3306',
    'database'  => 'database',
    'username'  => 'root',
    'password'  => 'password',
    'charset'   => 'utf8mb4',
    'options'   => [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ],
];

$db = ConnectionFactory::create($params);
```

## Example Queries

```php
$sql = 'SELECT * FROM `users`;';
$db->execute($sql);


$sql = 'SELECT * FROM `users` LIMIT ?;';
$db->execute($sql, [50]);


$sql = 'SELECT * FROM `users` LIMIT :limit;';
$db->execute($sql, ['limit' => 50]);


$sql = 'INSERT INTO `users` (`name`, `email`, `address`) VALUES (:name, :email, :address);';
$bindings = [
    'name'    => 'User Test',
    'email'   => 'test@test.com',
    'address' => 'UA',
];
$db->execute($sql, $bindings);


$sql = 'INSERT INTO `users` (`name`, `email`, `address`) VALUES (?, ?, ?);';
$bindings = [
    'User Test',
    'test@test.com',
    'UA',
];
$db->execute($sql, $bindings);
```
