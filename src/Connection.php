<?php

namespace R1KO\Database;

use R1KO\Database\Exceptions\DatabaseException;
use R1KO\Database\Contracts\IConnection;
use R1KO\Database\Contracts\IDriver;
use PDO;
use PDOException;
use PDOStatement;
use Closure;
use Throwable;

class Connection implements IConnection
{
    private PDO $pdo;
    private IDriver $driver;

    private array $types  = [
        'integer'   => PDO::PARAM_INT,
        'string'    => PDO::PARAM_STR,
        'NULL'      => PDO::PARAM_NULL,
        'boolean'   => PDO::PARAM_BOOL,
    ];

    private string $charset;

    public function __construct(PDO $pdo, IDriver $driver, array $params)
    {
        $this->pdo = $pdo;
        $this->driver = $driver;
        $this->charset = $params['charset'] ?? 'utf8';

        $driver->onInit($this);
    }

    public function getCharset(): string
    {
        return $this->charset;
    }

    public function getDriver(): IDriver
    {
        return $this->driver;
    }

    public function getPDO(): PDO
    {
        return $this->pdo;
    }

    // https://stackoverflow.com/questions/49641775/when-to-use-pdo-exec-vs-execute-vs-query
    // http://xfloyd.net/blog/?p=1047
    /*
     * Выполняет SQL-запрос и возвращает результирующий набор в виде объекта PDOStatement
     */
    public function query(string $sql): PDOStatement
    {
        try {
            return $this->pdo->query($sql, PDO::FETCH_ASSOC);
        } catch (PDOException $exception) {
            throw new DatabaseException(
                $exception->getMessage(),
                (int) $exception->getCode(),
                $exception
            );
        }
    }

    /*
     * Выполняет SQL-запрос и возвращает кол-во затронутых строк
     * (INSERT, UPDATE, DELETE)
     */
    public function exec(string $sql): int
    {
        try {
            return $this->pdo->exec($sql);
        } catch (PDOException $exception) {
            throw new DatabaseException(
                $exception->getMessage(),
                (int) $exception->getCode(),
                $exception
            );
        }
    }

//    private function runHandledQuery(callable $query)
//    {
//        try {
//            return $query();
//        } catch (PDOException $exception) {
//            throw new DatabaseException(
//                $exception->getMessage(),
//                $exception->getCode(),
//                $exception
//            );
//        }
//    }

    /*
     * Выполняет подготовку данных (prepare) и выполняет SQL-запрос. Возвращает объект PDOStatement
     *
     * @param string $sql
     * @param array|null $bind
     * @return PDOStatement|int
     */
    public function execute(string $sql, ?array $bind = null): PDOStatement
    {
        try {
            $statement = $this->pdo->prepare($sql);

            if ($bind) {
                $this->bindArrayValues($statement, $bind);
            }

            $statement->execute();

            return $statement;
        } catch (PDOException $exception) {
            throw new DatabaseException(
                $exception->getMessage(),
                (int) $exception->getCode(),
                $exception
            );
        }
    }

    public function executeIterable(string $sql, iterable $iterator): void
    {
        try {
            $statement = $this->pdo->prepare($sql);

            foreach ($iterator as $bind) {
                $this->bindArrayValues($statement, $bind);

                $statement->execute();
//                yield $this->getLastInsertId();
//                yield $statement->rowCount();
            }
        } catch (PDOException $exception) {
            throw new DatabaseException(
                $exception->getMessage(),
                (int) $exception->getCode(),
                $exception
            );
        }
    }

    public function prepare(string $sql): PDOStatement
    {
        return $this->pdo->prepare($sql);
    }

    public function executeStatement(PDOStatement $statement, ?array $bind = null): PDOStatement
    {
        if ($bind) {
            $this->bindArrayValues($statement, $bind);
        }

        $statement->execute();

        return $statement;
    }

    public function getLastInsertId(?string $aiColumn = null): int
    {
        return $this->pdo->lastInsertId($aiColumn);
    }

    public function quote(string $value, int $type = PDO::PARAM_STR): string
    {
        return $this->pdo->quote($value, $type);
    }

    public function inTransaction(): bool
    {
        return $this->pdo->inTransaction();
    }

    // $isolationLevel = false
    public function begin(): void
    {
        $this->pdo->beginTransaction();
    }

    public function commit(): void
    {
        $this->pdo->commit();
    }

    public function rollback(): void
    {
        $this->pdo->rollBack();
    }

    public function transaction(Closure $callback)
    {
        $this->begin();

        try {
            $result = $callback();

            $this->commit();
            return $result;
        } catch (Throwable $exception) {
            $this->rollback();

            throw $exception;
        }
    }

    private function bindArrayValues(PDOStatement $statement, array $array): void
    {
        foreach ($array as $key => $value) {
            $type = $this->getParamType($value);

            if (is_numeric($key)) {
                ++$key;
            } else {
                $key = ':' . $key;
            }

            $statement->bindValue($key, $value, $type);
        }
    }

    private function getParamType($param): int
    {
        $type = gettype($param);

        if (array_key_exists($type, $this->types)) {
            return $this->types[$type];
        }

        return PDO::PARAM_STR;
    }
}
