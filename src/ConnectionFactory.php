<?php

namespace R1KO\Database;

use R1KO\Database\Contracts\IConnection;
use R1KO\Database\Contracts\IConnector;
use R1KO\Database\Contracts\IDriver;
use InvalidArgumentException;
use R1KO\Database\Connection;
use R1KO\Database\Connectors\MySqlConnector;
use R1KO\Database\Connectors\PostgresConnector;
use R1KO\Database\Connectors\SQLiteConnector;
use R1KO\Database\Connectors\SqlServerConnector;
use R1KO\Database\Drivers\SQLiteDriver;
use R1KO\Database\Drivers\MySqlDriver;
use PDO;

class ConnectionFactory
{
    protected const DRIVER_TO_CONNECTOR_MAPPER = [
        'sqlite' => SQLiteConnector::class,
        'mysql'  => MySqlConnector::class,
        // TODO: add support this drivers
//        'pgsql'  => PostgresConnector::class,
//        'sqlsrv' => SqlServerConnector::class,
    ];

    protected const DRIVER_TO_CLASS_MAPPER = [
        'sqlite' => SQLiteDriver::class,
        'mysql'  => MySqlDriver::class,
        // TODO: add support this drivers
//        'pgsql'  => PostgresConnector::class,
//        'sqlsrv' => SqlServerConnector::class,
    ];

    final public function __construct()
    {
    }

    public static function create(array $params): IConnection
    {
        $factoryInstance = new static();
        return $factoryInstance->createByParams($params);
    }

    public function createByParams(array $params): IConnection
    {
        $driverName = $this->getDriver($params);

        if (!in_array($driverName, PDO::getAvailableDrivers(), true)) {
            throw new InvalidArgumentException(sprintf('Driver PDO "%s" not found!', $driverName));
        }

        $connector = static::factoryConnectorInstance($driverName);

        $dsn = $connector->getDsn($params);
        $pdo = $connector->createPDOInstance($dsn, $params);

        $driver = $this->factoryDriverInstance($driverName);
        $connection = $this->factoryConnectionInstance($pdo, $driver, $params);

        return $connection;
    }

    private function getDriver(array $params): string
    {
        if (!array_key_exists('driver', $params)) {
            throw new InvalidArgumentException('Param "driver" not found!');
        }

        return strtolower($params['driver']);
    }

    public function factoryConnectorInstance(string $driverName): IConnector
    {
        if (!array_key_exists($driverName, static::DRIVER_TO_CONNECTOR_MAPPER)) {
            throw new InvalidArgumentException(sprintf('Connector for driver "%s" not found!', $driverName));
        }

        $connectorClass = static::DRIVER_TO_CONNECTOR_MAPPER[$driverName];
        return new $connectorClass();
    }

    public function factoryDriverInstance(string $driverName): IDriver
    {
        if (!array_key_exists($driverName, static::DRIVER_TO_CLASS_MAPPER)) {
            throw new InvalidArgumentException(sprintf('Driver "%s" not found!', $driverName));
        }

        $driverClass = static::DRIVER_TO_CLASS_MAPPER[$driverName];
        return new $driverClass();
    }

    public function factoryConnectionInstance(PDO $pdo, IDriver $driver, array $params): IConnection
    {
        return new Connection($pdo, $driver, $params);
    }
}
