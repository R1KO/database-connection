<?php

namespace R1KO\Database\Connectors;

use R1KO\Database\Contracts\IConnector;
use R1KO\Database\Exceptions\DatabaseException;
use InvalidArgumentException;
use PDOException;
use PDO;

abstract class AbstractConnector implements IConnector
{
    protected array $options = [
        PDO::ATTR_CASE              => PDO::CASE_NATURAL,
        PDO::ATTR_ERRMODE           => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_ORACLE_NULLS      => PDO::NULL_NATURAL,
        PDO::ATTR_STRINGIFY_FETCHES => false,
        PDO::ATTR_EMULATE_PREPARES  => false,
    ];

    public function createPDOInstance(string $dsn, array $params): PDO
    {
        $params = $this->getPreparedParams($params);
        $options = $this->getPdoOptions($params);

        try {
            return $this->factoryPdoConnection(
                $dsn,
                $params['user'],
                $params['password'],
                $options
            );
        } catch (PDOException $e) {
            throw new DatabaseException($e->getMessage(), $e->getCode(), $e);
        }
    }

    protected function getPreparedParams(array $params): array
    {
        return $params;
    }

    protected function getPdoOptions(array $params): array
    {
        $options = $this->options;

        if (isset($params['options']) && is_array($params['options'])) {
            foreach ($params['options'] as $option => $value) {
                $options[$option] = $value;
            }
        }

        return $options;
    }

    protected function factoryPdoConnection(string $dsn, ?string $username, ?string $password, array $options): PDO
    {
        // https://www.php.net/manual/en/features.persistent-connections.php
        /*if (class_exists(PDOConnection::class) && ! $this->isPersistentConnection($options)) {
            return new PDOConnection($dsn, $username, $password, $options);
        }*/

        return new PDO($dsn, $username, $password, $options);
    }

    protected function getRequiredParam(array $params, string $key)
    {
        if (!array_key_exists($key, $params)) {
            throw new InvalidArgumentException("Param '{$key}' not found!");
        }

        return $params[$key];
    }

    protected function getParam(array $params, string $key, $default)
    {
        if (!array_key_exists($key, $params)) {
            return $default;
        }

        return $params[$key];
    }
}
