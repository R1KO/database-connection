<?php

namespace R1KO\Database\Connectors;

use PDO;

class MySqlConnector extends AbstractConnector
{
    protected const DEFAULT_CHARSET = 'utf8';
    protected const DEFAULT_PORT    = 3306;

    protected function getPreparedParams(array $params): array
    {
        $params['user'] = $this->getRequiredParam($params, 'user');
        $params['password'] = $this->getRequiredParam($params, 'password');

        return $params;
    }

    public function getDsn(array $params): string
    {
        return sprintf(
            'mysql:host=%s;port=%s;dbname=%s;charset=%s',
            $this->getRequiredParam($params, 'host'),
            $this->getParam($params, 'port', static::DEFAULT_PORT),
            $this->getRequiredParam($params, 'name'),
            $this->getParam($params, 'charset', static::DEFAULT_CHARSET)
        );
    }
}
