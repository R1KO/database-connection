<?php

namespace R1KO\Database\Connectors;

use PDO;

class SQLiteConnector extends AbstractConnector
{
    protected function getPreparedParams(array $params): array
    {
        $params['user'] = null;
        $params['password'] = null;

        return $params;
    }

    public function getDsn(array $params): string
    {
        return sprintf('sqlite:%s', $this->getRequiredParam($params, 'path'));
    }
}
