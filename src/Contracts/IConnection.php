<?php

namespace R1KO\Database\Contracts;

use R1KO\Database\Contracts\IDriver;
use PDO;
use PDOStatement;

interface IConnection
{
    public function getCharset(): string;
    public function getDriver(): IDriver;
    public function getPDO(): PDO;

    public function quote(string $value): string;

    public function query(string $sql): PDOStatement;
    public function exec(string $sql): int;
    public function execute(string $sql, ?array $bind = null): PDOStatement;
    public function executeIterable(string $sql, iterable $iterator): void;
    public function prepare(string $sql): PDOStatement;
    public function executeStatement(PDOStatement $statement, ?array $bind = null): PDOStatement;

    public function getLastInsertId(?string $aiColumn = null): int;

    public function inTransaction(): bool;
    public function begin(): void;
    public function commit(): void;
    public function rollback(): void;
}
