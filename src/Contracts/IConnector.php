<?php

namespace R1KO\Database\Contracts;

use PDO;
use PDOStatement;

interface IConnector
{
    public function getDsn(array $params): string;
    public function createPDOInstance(string $dsn, array $params): PDO;
}
