<?php

namespace R1KO\Database\Contracts;

use R1KO\Database\Contracts\IConnection;

interface IDriver
{
    public function onInit(IConnection $db): void;
    public function quoteTableName(string $name): string;
    public function quoteColumnName(string $name): string;
}
