<?php

namespace R1KO\Database\Drivers;

use R1KO\Database\Contracts\IDriver;
use R1KO\Database\Contracts\IConnection;

abstract class AbstractDriver implements IDriver
{
    protected const ESCAPE_SYMBOL = '`';
    protected const SEPARATOR_SYMBOL = '.';
//
//    private $db;
//
//    public function __construct(IConnection $db)
//    {
//        $this->db = $db;
//
//        $this->onInit();
//    }

//    protected function onInit(IConnection $db): void
//    {
//        $sql = sprintf('SET NAMES \'%s\'', $this->db->getCharset());
//        $this->db->query($sql);
//    }
    public function onInit(IConnection $db): void
    {
    }

    public function quoteTableName(string $name): string
    {
        return static::ESCAPE_SYMBOL . $name . static::ESCAPE_SYMBOL;
    }

    public function quoteColumnName(string $name): string
    {
        if (strpos($name, static::SEPARATOR_SYMBOL) !== false) {
            $name = str_replace(
                static::SEPARATOR_SYMBOL,
                static::ESCAPE_SYMBOL . static::SEPARATOR_SYMBOL . static::ESCAPE_SYMBOL,
                $name
            );
        }

        return static::ESCAPE_SYMBOL . $name . static::ESCAPE_SYMBOL;
    }
}
