<?php

namespace R1KO\Database\Drivers;

use R1KO\Database\Contracts\IConnection;
use R1KO\Database\Drivers\AbstractDriver;

class MySqlDriver extends AbstractDriver
{
    public function onInit(IConnection $db): void
    {
        $sql = sprintf('SET NAMES \'%s\'', $db->getCharset());
        $db->query($sql);
    }
}
