<?php

namespace Tests;

use PDO;
use R1KO\Database\Contracts\IConnection;
use R1KO\Database\Connection;
use R1KO\Database\ConnectionFactory;
use R1KO\Database\Drivers\SQLiteDriver;

class ConnectionTest extends TestCase
{
    public function testConnection(): void
    {
        $params = $this->getDefaultParams();
        $driver = new SQLiteDriver();
        $pdo = new PDO('sqlite::memory:');

        $db = new Connection($pdo, $driver, $params);

        $this->assertInstanceOf(IConnection::class, $db);
        $this->assertInstanceOf(Connection::class, $db);
    }

    public function testConnectionByFactory(): void
    {
        $db = ConnectionFactory::create($this->getDefaultParams());

        $this->assertInstanceOf(IConnection::class, $db);
        $this->assertInstanceOf(Connection::class, $db);
    }
}
