<?php

namespace Tests;

use R1KO\Database\Contracts\IConnection;
use R1KO\Database\Exceptions\DatabaseException;

class ExecTest extends TestCase
{
    public function testSuccessQuery(): void
    {
        $result = $this->getConnection()->exec($this->getCreateContactsTableSql());

        $this->assertEquals(0, $result);
    }

    public function testSuccessCountQuery(): void
    {
        $this->createContactsTable();

        $result = $this->getConnection()->exec($this->getInsertContactSql());
        $this->assertEquals(1, $result);
    }

    public function testFailQuery(): void
    {
        $this->expectException(DatabaseException::class);
        $this->getConnection()->exec('MISSING QUERY');
    }
}
