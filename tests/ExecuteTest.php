<?php

namespace Tests;

use R1KO\Database\Contracts\IConnection;
use R1KO\Database\Exceptions\DatabaseException;

class ExecuteTest extends TestCase
{
    private function getSelectSqlWithPlaceholders(): string
    {
        return 'SELECT * FROM contacts WHERE email = :email';
    }

    private function getSelectSqlWithParams(): string
    {
        return 'SELECT * FROM contacts WHERE email = ?';
    }

    private function getInsertSql(): string
    {
        return <<<END
            INSERT INTO contacts (contact_id, first_name, last_name, email, phone) 
            VALUES (:contact_id, :first_name, :last_name, :email, :phone);
        END;
    }

    public function testSuccessQueryWithPlaceholders(): void
    {
        $this->createContactsTable();
        $count = 5;
        $contacts = $this->createContacts($count);

        $email = $contacts[0]['email'];

        $bind = [
            'email' => $email,
        ];

        $statement = $this->getConnection()->execute($this->getSelectSqlWithPlaceholders(), $bind);

        $results = [];

        foreach ($statement as $row) {
            $results[] = $row;
        }

        $this->assertCount(1, $results);
    }

    public function testSuccessQueryWithParams(): void
    {
        $this->createContactsTable();
        $count = 5;
        $contacts = $this->createContacts($count);

        $email = $contacts[0]['email'];

        $bind = [
            $email,
        ];

        $statement = $this->getConnection()->execute($this->getSelectSqlWithPlaceholders(), $bind);

        $results = [];

        foreach ($statement as $row) {
            $results[] = $row;
        }

        $this->assertCount(1, $results);
    }

    public function testInsertQuery(): void
    {
        $this->createContactsTable();

        $faker = $this->getFaker();
        $bind = [
            'contact_id' => $faker->randomNumber(8, true),
            'first_name' => $faker->firstName(),
            'last_name'  => $faker->lastName(),
            'email'      => $faker->safeEmail(),
            'phone'      => $faker->tollFreePhoneNumber(),
        ];

        $statement = $this->getConnection()->execute($this->getInsertSql(), $bind);
        $this->assertEquals(1, $statement->rowCount());
    }

    public function testFailQuery(): void
    {
        $this->expectException(DatabaseException::class);
        $this->getConnection()->execute('MISSING QUERY');
    }
}
