<?php

namespace Tests;

use R1KO\Database\Contracts\IConnection;
use R1KO\Database\Exceptions\DatabaseException;

class QueryTest extends TestCase
{
    private function getSelectSql(): string
    {
        return 'SELECT * FROM contacts';
    }

    public function testSuccessQuery(): void
    {
        $this->createContactsTable();
        $count = 5;
        $this->createContacts($count);

        $statement = $this->getConnection()->query($this->getSelectSql());

        $results = [];

        foreach ($statement as $row) {
            $results[] = $row;
        }

        $this->assertCount($count, $results);
    }

    public function testInsertQuery(): void
    {
        $this->createContactsTable();

        $statement = $this->getConnection()->query($this->getInsertContactSql());
        $this->assertEquals(1, $statement->rowCount());
    }

    public function testFailQuery(): void
    {
        $this->expectException(DatabaseException::class);
        $this->getConnection()->query('MISSING QUERY');
    }
}
