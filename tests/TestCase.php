<?php

namespace Tests;

use Exception;
use Faker\Factory;
use PHPUnit\Framework\TestCase as BaseTestCase;
use R1KO\Database\Contracts\IConnection;
use R1KO\Database\ConnectionFactory;
use PDO;

class TestCase extends BaseTestCase
{
    /**
     * @var IConnection
     */
    protected $db;

    protected function setUp(): void
    {
        $this->db = $this->createConnection();
    }

    protected function createConnection(): IConnection
    {
        return ConnectionFactory::create($this->getDefaultParams());
    }

    protected function getConnection(): IConnection
    {
        return $this->db;
    }

    protected function getDefaultParams(): array
    {
        return [
            'driver'   => $_ENV['DB_DRIVER'],
            'host'     => $_ENV['DB_HOST'],
            'port'     => $_ENV['DB_PORT'],
            'name'     => $_ENV['DB_NAME'],
            'user'     => $_ENV['DB_USER'],
            'password' => $_ENV['DB_PASSWORD'],
            'charset'  => $_ENV['DB_CHARSET'],
            'path'     => $_ENV['DB_PATH'],
        ];
    }

    protected function getDriverName(): string
    {
        return $this->db->getPDO()->getAttribute(PDO::ATTR_DRIVER_NAME);
    }

    protected function getFaker()
    {
        return Factory::create();
    }

    protected function getCreateContactsTableSql(): string
    {
        $driver = $this->getDriverName();
        if ($driver === 'sqlite') {
            return <<<END
                CREATE TABLE contacts (
                    contact_id INTEGER PRIMARY KEY AUTOINCREMENT,
                    first_name TEXT NOT NULL,
                    last_name TEXT NOT NULL,
                    email TEXT NOT NULL,
                    phone TEXT NOT NULL
                );
            END;
        }
        if ($driver === 'mysql') {
            return <<<END
                CREATE TABLE contacts (
                    contact_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    first_name varchar(64) NOT NULL,
                    last_name varchar(64) NOT NULL,
                    email varchar(64) NOT NULL,
                    phone varchar(32) NOT NULL
                );
            END;
        }

        throw new Exception(
            sprintf('[%s] Not found query for driver "%s"', __METHOD__, $driver)
        );
    }

    protected function getInsertContactSql(): string
    {
        $db = $this->getConnection();
        $faker = $this->getFaker();

        return <<<END
            INSERT INTO contacts (first_name, last_name, email, phone) 
            VALUES (
                    {$db->quote($faker->firstName())},
                    {$db->quote($faker->lastName())},
                    {$db->quote($faker->safeEmail())},
                    {$db->quote($faker->tollFreePhoneNumber())}
                    );
        END;
    }

    protected function createContactsTable(): void
    {
        $result = $this->getConnection()->exec('DROP TABLE IF EXISTS contacts');
        $this->assertEquals(0, $result);

        $result = $this->getConnection()->exec($this->getCreateContactsTableSql());
        $this->assertEquals(0, $result);
    }

    protected function createContacts(int $count): array
    {
        $db = $this->getConnection();
        $faker = $this->getFaker();

        $sql = 'INSERT INTO contacts ( first_name, last_name, email, phone) VALUES ';
        $values = [];
        $rows = [];
        foreach (range(1, $count) as $i) {
            $rowValues = [
                'first_name' => $faker->firstName(),
                'last_name'  => $faker->lastName(),
                'email'      => $faker->safeEmail(),
                'phone'      => $faker->tollFreePhoneNumber(),
            ];
            $values[] = $rowValues;

            $rows[] = sprintf(
                "(%s, %s, %s, %s)",
                $db->quote($rowValues['first_name']),
                $db->quote($rowValues['last_name']),
                $db->quote($rowValues['email']),
                $db->quote($rowValues['phone'])
            );
        }

        $sql .= implode(', ', $rows);

        $result = $db->exec($sql);
        $this->assertEquals($count, $result);

        return $values;
    }
}
